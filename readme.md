# startkafa

Uma aplicacão simples para demonstrar o uso do confluent kafka

# Tecnologias

1. Springboot
2. Spring Kafka
3. Gradle
4. Java JDK 1.8
5. Confluent Kafka

# Utilizacão
Para fazer pooling das imagens do docker e executar os containers
```
 docker-compose up -d --build
```
 
E vá tomar um café !  :D
 
Para executar essa aplicacão

```
 ./gradlew clean build
 java -jar build/libs/startkafka-0.0.1-SNAPSHOT.jar 
```
Acessar o control center http://localhost:9021/
 
## INFO | IMPORTANTE |
**Ao acessar o Control Center http://localhost:9021/ pode ser que demore um ou dois minutos para o cluster ficar online.   
Nesse tempo pode ser que o cluster não apareca ou apareca como unhealthy, aguarde um pouco e tudo ficará bem ! :D**

## Testando

### Para iniciar
Uma vez que o cluster estiver online 
1. Clique sobre o cluster 
2. Clique em *Topics*
3. Clique em *Add a Topic*
4. Em *Topic name* escreva helloworld.t  
5. Por fim clique em *Create with defaults*
6. Para enviar uma mensagem
```
curl -X POST \
  'http://localhost:8080/kafka/publish?message=JAVA' \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 0' \
  -H 'Host: localhost:8080' \
  -H 'Postman-Token: 2cec60be-bab7-40d1-8498-e3efd9fd63d8,7bf5bf08-278a-4927-9d88-dbf91bcbd5d7' \
  -H 'User-Agent: PostmanRuntime/7.17.1' \
  -H 'cache-control: no-cache'
```
7. Para acompanhar as mensagens cique sobre o tópico e clique na aba *Messages*

OBS: As mensagens também podem ser vistas em logs de console

### Para encerrar

Para encerrar a aplicacão springboot basta pressionar CTRL+C

Para encerrar os containers docker

1. O comando abaixo lista os ids dos containers em execucão
```
docker container ls -aq
```

2. Execute o seguinte comando para parar os contêineres do Docker para Confluent:
```
docker container stop $(docker container ls -a -q -f "label=io.confluent.docker")
```

3. **WARNING** - OPCIONAL EXECUTE APENAS PARA EXCLUIR AS IMAGENS 

Execute os seguintes comandos para parar os contêineres e podar o sistema Docker. A execução desses comandos exclui contêineres, redes, volumes e imagens; liberando espaço em disco:

```
docker container stop $(docker container ls -a -q -f "label=io.confluent.docker") && docker system prune -a -f --volumes
```

OBS: Para verificar os containers em execucao

```
docker ps
``` 

## REFERÊNCIAS

https://docs.confluent.io/current/getting-started.html

https://docs.confluent.io/current/quickstart/ce-docker-quickstart.html#ce-docker-quickstart
