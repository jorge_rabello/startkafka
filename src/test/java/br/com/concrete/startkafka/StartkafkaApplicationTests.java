package br.com.concrete.startkafka;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.concrete.startkafka.services.Receiver;
import br.com.concrete.startkafka.services.Sender;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@EmbeddedKafka(partitions = 1, topics = {StartkafkaApplicationTests.HELLO_WORLD_TOPIC})
public class StartkafkaApplicationTests {

  static final String HELLO_WORLD_TOPIC = "helloworld.t";

  @Autowired
  private Receiver receiver;
  @Autowired
  private Sender sender;

  @Test
  public void testReceive() throws Exception {
    sender.send("Hello Spring Kafka !");

    receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
    assertThat(receiver.getLatch().getCount()).isEqualTo(0);
  }
}
