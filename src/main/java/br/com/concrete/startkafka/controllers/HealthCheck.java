package br.com.concrete.startkafka.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/test")
public class HealthCheck {

  @GetMapping(value = "/health")
  public String health() {
    return "It Works !";
  }

}
