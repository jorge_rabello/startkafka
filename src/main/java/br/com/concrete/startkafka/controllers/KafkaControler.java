package br.com.concrete.startkafka.controllers;

import br.com.concrete.startkafka.services.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/kafka")
public class KafkaControler {

  private final Sender sender;

  @Autowired
  public KafkaControler(Sender sender) {
    this.sender = sender;
  }

  @PostMapping(value = "/publish")
  public void sendMessageToKafkaTopic(@RequestParam("message") String message) {
    this.sender.send(message);
  }
}
