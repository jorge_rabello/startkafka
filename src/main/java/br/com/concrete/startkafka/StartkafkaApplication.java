package br.com.concrete.startkafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartkafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(StartkafkaApplication.class, args);
	}

}
